#define _CRT_SECURE_NO_WARNINGS
#define MAX_NUMBER_OF_INTERMIDIATE_CLUSTERS 100
#include <iostream>
#include <fstream>
#include <map>
#include <string.h>
#include <vector>

std::ifstream fin("file.txt");
std::ofstream fout("out.txt");

struct Pixel {
	int R;
	int G;
	int B;
	Pixel(int R, int G, int B) :R(R), G(G), B(B) {}
	Pixel() = default;
	friend auto  operator<<(std::ostream& o, const Pixel& p) -> std::ostream& {
		o << "[ " << p.R << " , " << p.G << " , " << p.B << " ] " << std::endl;
		return o;
	}
};
struct Point {
	int x;
	int y;
	Point(int x = 0, int y = 0) :x(x), y(y) {}
	friend auto operator<<(std::ostream& o, const Point& p)->std::ostream& {
		o << "[ " << p.x << " , " << p.y << " ] =  ";
		return o;
	}
	auto operator<(const Point &rhs)const->const bool {
		return (x < rhs.x) || ((x == rhs.x) && (y < rhs.y));
	}
	auto operator>(const Point& other) const->const bool {
		const auto& ret = (x < other.x) && (y < other.y);
		return !ret;
	}
	auto operator==(const Point& other) const->const bool {
		return (x == other.x) && (y == other.y);
	}
	auto operator!=(const Point& other) const->const bool {
		return (x != other.x) && (y != other.y);
	}
};
auto distanta(const Pixel& one, const  Pixel& another) ->double {
	return std::sqrt((one.R - another.R)* (one.R - another.R) + (one.G - another.G)*(one.G - another.G) + (one.B - another.B)*(one.B - another.B));
}

class KMap {
private:
	unsigned int numberOfClusters;
	unsigned int numberOfIterations;
	std::map<Point, Pixel> allPoints;
	std::vector<std::map<Point, Pixel>> clusters;
	std::vector<Point>centres;
	std::vector<Pixel> centroids;
private:
	auto calculateCentroid(unsigned int index, const Pixel& element, const int size, bool minus = false) {
		if (size != 0)
		{
			centroids[index].R = (centroids[index].R + element.R) / size;
			centroids[index].G = (centroids[index].G + element.G) / size;
			centroids[index].B = (centroids[index].B + element.B) / size;
			if (minus) {
				centroids[index].R = (centroids[index].R - element.R) / size;
				centroids[index].G = (centroids[index].G - element.G) / size;
				centroids[index].B = (centroids[index].B - element.B) / size;
			}
		}
	}
	auto readFromFile()->std::map<Point, Pixel> {
		char line[256] = { '\0' };
		fin.getline(line, 256);
		while (fin.getline(line, 256)) {
			Pixel pixel;
			Point point;
			char * p = strtok(line, "[ ,.]()=");

			int k = 0;
			while (p != nullptr) {
				switch (k) {
				case 0:
					point.x = atoi(p);
					break;
				case 1:
					point.y = atoi(p);
					break;
				case 2:
					pixel.R = atoi(p);
					break;
				case 3:
					pixel.G = atoi(p);
					break;
				case 4:
					pixel.B = atoi(p);
					break;
				}
				p = strtok(nullptr, "[ ,.]()=");
				++k;
			}
			allPoints.insert(std::make_pair(point, pixel));
		}
		return allPoints;
	}
	auto assignRandomCenters()->void {
		centres.push_back(Point(2, 1));
		centres.push_back(Point(50, 70));
	}
	auto assignCentroids()->void {
		centroids.push_back(allPoints[centres[0]]);
		centroids.push_back(allPoints[centres[1]]);
	}
	auto assignFirstElementsToClusters()->void {
		clusters[0].emplace(centres[0], centroids[0]);
		clusters[1].emplace(centres[1], centroids[1]);
	}
	auto divideIntoClusters()->void {

		for (auto& element : allPoints) {
			const auto point = clusters[0][centres[0]];
			const auto point2 = clusters[1][centres[1]];
			auto to_cl1 = distanta(point, element.second);
			auto to_cl2 = distanta(point2, element.second);

			if (to_cl1 < to_cl2) {
				clusters[0].emplace(element.first, element.second);
				calculateCentroid(0, element.second, clusters[0].size());
			}
			else {
				clusters[1].emplace(element.first, element.second);
				calculateCentroid(1, element.second, clusters[1].size());
			}
		}

	}
	auto relocateInClustersByNewDistance()->void {
		for (auto i = 0; i < numberOfIterations; i++) {
			auto size_cl3 = clusters[i + 1].size();
			auto size_cl2 = clusters[i].size();
			for (const auto& element : clusters[i + 1]) {
				if (distanta(element.second, centroids[0]) < distanta(element.second, centroids[1])) {
					clusters[i + 2].emplace(element);
					calculateCentroid(1, element.second, size_cl3 - 1, true);
					size_cl3--;
				}
				else {
					clusters[i + 3].emplace(element);
				}
			}
			for (const auto& element : clusters[i]) {
				if (distanta(element.second, centroids[1]) < distanta(element.second, centroids[0])) {
					clusters[i + 3].emplace(element);
					calculateCentroid(0, element.second, size_cl2 - 1, true);
					size_cl2--;
				}
				else {
					clusters[i + 2].emplace(element);
				}
			}
		}
	}
	auto composeMapToBeWritten()->std::map<Point, Pixel> {
		std::map<Point, Pixel> mapToBeWritten;

		for (auto i = 0; i < clusters.size(); i++) {
			for (auto& element : clusters[i]) {
				element.second.R = 0;
				element.second.G = 0;
				element.second.B = i * 200;
				mapToBeWritten.emplace(element);
			}
		}

		return mapToBeWritten;
	}
	auto print()->void {
		auto mapToBeWritten = composeMapToBeWritten();
		for (const auto& pair : mapToBeWritten) {
			fout << pair.first << pair.second;
		}
	}

public:
	KMap(unsigned int numberOfClusters, unsigned int numberOfIterations) :numberOfClusters(numberOfClusters), numberOfIterations(numberOfIterations) {
		clusters = std::vector<std::map<Point, Pixel>>(MAX_NUMBER_OF_INTERMIDIATE_CLUSTERS);
		centres = std::vector<Point>();
		centroids = std::vector<Pixel>();
	}
	auto startAlgorithm()->void {
		readFromFile();
		assignRandomCenters();
		assignCentroids();
		assignFirstElementsToClusters();
		divideIntoClusters();
		relocateInClustersByNewDistance();
		print();
	}
};
auto main()->int
{
	KMap kmap(2, 20);
	kmap.startAlgorithm();

	return 0;
}